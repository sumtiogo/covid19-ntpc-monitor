chrome.alarms.create({ periodInMinutes: 1 });

async function reload() {
  let tabs = await chrome.tabs.query({
    url: "https://docs.google.com/spreadsheets/u/0/d/e/2PACX-1vRamtpfvNeHjvIhrhOpE6pqu_7HjNobNqznYqL8kna7DAj0g5yJg2Lvy1kDKVA0NaouHB2ygQ6tffiT/pubhtml/sheet?headers=false&gid=1117416164",
  });
  await chrome.tabs.reload(tabs[0].id);

  tabs = await chrome.tabs.query({
    title: "疫苖線上預約-新北市衛生局",
  });
  await chrome.tabs.reload(tabs[0].id);
}
reload()

chrome.alarms.onAlarm.addListener(async () => {
  await reload();
});
